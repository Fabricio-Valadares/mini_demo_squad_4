import { Component } from "react";
import "./style.css";

export default class SectionButton extends Component {
  render() {
    return <div>{this.props.children}</div>;
  }
}
