import { Component } from "react";
import "./style.css";

export default class SectionImage extends Component {
  render() {
    return (
      <div className="image">
        <h1 className="title">Clique na imagem para baixar !</h1>
        <a href={this.props.linkIma} download>
          <img src={this.props.linkIma} alt="imagemRandom"></img>
        </a>
      </div>
    );
  }
}
