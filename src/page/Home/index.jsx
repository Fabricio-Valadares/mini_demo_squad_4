import { Component } from "react";
import Footer from "../../components/Footer";
import SectionButton from "../../components/SectionButton";
import SectionImage from "../../components/SectionImage";
import TextButton from "../../components/TextButton";
import "./style.css";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      linkIma: "",
    };
  }

  mudarState = () => {
    this.setState({
      linkIma: `./assets/${Math.floor(Math.random() * (100 + 0) - 0)}.jpg`,
    });
  };

  // components
  // state
  // props
  // pensar progaramação funcional

  render() {
    return (
      <main className="main">
        <SectionButton>
          <TextButton mudarState={this.mudarState} />
          {this.state.linkIma && <Footer />}
        </SectionButton>
        <SectionImage linkIma={this.state.linkIma}></SectionImage>
      </main>
    );
  }
}
